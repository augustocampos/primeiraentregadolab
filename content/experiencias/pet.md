+++
title = 'PET mecatrônica'
date = 2024-10-12T16:00:10-03:00
draft = false
+++
- Desenvolvi experiência prática em projetos mecatrônicos;
- Coordenei projetos de longa duração e multidisciplinares, focados em pesquisa, extensão e ensino;
- Liderei o projeto "Conhecendo a Mecatrônica", que envolveu a gestão de uma equipe e atendimento a um amplo público de alunos do ensino médio;
- Aprimorei minha visão holística na gestão de projetos; 
- Aprimorei minhas habilidades de comunicação, trabalho em equipe e liderança.