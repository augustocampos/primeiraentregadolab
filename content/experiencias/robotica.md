+++
title = 'Grupo de Robótica'
date = 2024-10-12T16:00:03-03:00
draft = false
+++
- Submeti 3 Artigos para a Mostra Nacional de robótica, todos posteriormente premiados com bolsas de iniciação científica Junior;
- Participação em competições OBR e TRIF 
- Líder de equipe durante 2 anos;
- 3°Lugar nacional na Olimpíadas Brasileiras de Robótica na categoria Programação e participação na FEBRACE- 2021; 
- 1° Lugar na Mostra Científica e Cultural do IFSP (MOCCIF), categoria engenharia - 2021;
- Além das capacidades técnicas, por meio do trabalho na equipe foi possível desenvolver a solução de problemas e a comunicação.