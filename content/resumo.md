+++
title = 'Sobre mim'
date = 2024-10-09T14:56:20-03:00
draft = false
+++

Tenho 20 anos e sou apaixonado por tecnologia, com um forte interesse em pesquisa, inovação e desenvolvimento. Tenho uma grande atração pela automatização de tarefas e pela maximização da produtividade. Além disso, sou motivado pelo aprendizado contínuo e pela superação de novos desafios. Estou em busca de oportunidades para adquirir experiência e crescer profissionalmente, contribuindo para o sucesso da empresa.